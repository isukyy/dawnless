﻿/*
 * Klasse zum handeln des Spielers
 * 
 *(c) 2014-2016 by Alexander Chabowski
 */
using UnityEngine;
using System.Collections;

//Werte des Spieler wie Leben, Mana, Gold etc...
struct PlayerValues
{
	public int health;
	public int mana;
	public int gold;
	public int damage;
	public float speed;
	public bool isAttacking;
	public bool canWalk;

	//TODO: isInMenu integrieren
	public bool isInMenu;
};

//Klasse des Spielers
public enum PlayerType
{
	Warrior,
	Archer,
	Mage,
	Dark_Assasine,
	Magic_Assasine,
	Skeleton,
	Knight,
	Rogue
};

public class Player : MonoBehaviour 
{
	private SpriteRenderer sprRenderer;

	//Elternobjekt
	public GameObject playerObject;

	private GameObject[] Shops;

	private PlayerValues pValues;

	public Sprite currentSprite;

    public float Movespeed;

	//Sprites des Magiers
	public Sprite player_left_spr,
				  player_right_spr,
				  player_up_spr,
				  player_down_spr;

	void Start () 
	{
		sprRenderer = transform.GetComponent<SpriteRenderer>();

		//Varibalen initialisieren
		pValues = new PlayerValues ();

		//Variablen zuweisen
		pValues.health = 100;
		pValues.mana = 50;
		pValues.speed = Movespeed;
		pValues.canWalk = true;
        pValues.isAttacking = false;
        pValues.damage = 0;
        pValues.isInMenu = false;
	}
	

	void Update () 
	{
		//Sendet den jeweiligen Lebenspunktewert an die MainCamera und zeigt es auf dem HUD an
		GameObject.FindGameObjectWithTag ("MainCamera").SendMessage ("getHealthAmount", pValues.health, SendMessageOptions.DontRequireReceiver);
		//Sendet den jeweiligen Manawert an die MainCamera und zeigt es auf dem HUD an
		GameObject.FindGameObjectWithTag ("MainCamera").SendMessage ("getManaAmount",   pValues.mana, SendMessageOptions.DontRequireReceiver);
		//Sendet den jeweiligen Goldwert an die MainCamera und zeigt es auf dem HUD an
		GameObject.FindGameObjectWithTag ("MainCamera").SendMessage ("getGoldAmount",   pValues.gold, SendMessageOptions.DontRequireReceiver);

        //Sendet den aktuellen Wert des Goldes an die Shops damit geprüft werden kann ob der Spieler
        //sich dieses Obje
        sendCurrentGoldToShops();

	}

	void FixedUpdate()
	{
		HandleMovement(pValues.canWalk);
	}

	//Handelt den Input und die Bewegung vom Character
	void HandleMovement(bool canMove)
	{
		if (canMove) 
		{
			if (Input.GetKey (KeyCode.D) || Input.GetAxis ("Horizontal") >= 0.2f) 
			{
					playerObject.transform.Translate (Vector3.right * pValues.speed * Time.deltaTime);
					sprRenderer.sprite = (Sprite)player_right_spr;
			}

			if (Input.GetKey (KeyCode.A) || Input.GetAxis ("Horizontal") <= -0.2f) 
			{
					playerObject.transform.Translate (Vector3.left * pValues.speed * Time.deltaTime);
					sprRenderer.sprite = (Sprite)player_left_spr;
			}

			if (Input.GetKey (KeyCode.W) || Input.GetAxis ("Vertical") >= 0.2f) 
			{
					playerObject.transform.Translate (Vector3.up * pValues.speed * Time.deltaTime);
					sprRenderer.sprite = (Sprite)player_up_spr;
			}

			if (Input.GetKey (KeyCode.S) || Input.GetAxis ("Vertical") <= -0.2f) 
			{
					playerObject.transform.Translate (Vector3.down * pValues.speed * Time.deltaTime);
					sprRenderer.sprite = (Sprite)player_down_spr;
			}
		}
		transform.Translate (Vector3.zero);
	}


    private void sendCurrentGoldToShops()
    {
        Shops = GameObject.FindGameObjectsWithTag("Shop");

        for (int i = 0; i < Shops.Length; ++i)
        {
            if (GameObject.FindGameObjectWithTag("Shop") != null)
            {
                Shops[i].SendMessage("getGoldAmount", pValues.gold, SendMessageOptions.DontRequireReceiver);
            }
        }
    }

	//Erhöht den Gesundheitswert
	public void addHealth(int amount)
	{
		pValues.health += amount;
	}

	//Senkt den jeweiligen Gesundheitswert
	public void decreaseHealth(int amount)
	{
		pValues.health -= amount;
	}

	//Fügt Mana hinzu
	public void addMana(int amount)
	{
		pValues.mana += amount;
	}

	//Senkt den jeweiligen Manawert
	public void decreaseMana(int amount)
	{
		pValues.mana -= amount;
	}

	//Fügt Gold hinzu
	public void addGold(int amount)
	{
		pValues.gold += amount;
	}

	//Senkt den jeweiligen Goldwert
	public void decreaseGold(int amount)
	{
		pValues.gold -= amount;
	}
	
}
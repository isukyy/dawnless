﻿/*
 *  Diese Klasse ist für das HUD des Spielers zuständig
 * 
 *(c) 2014-2016 by Alexander Chabowski
 */


using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour 
{

	private int goldAmount, healthAmount, manaAmount;
	public Texture2D heart, manaDrop;
	public Texture2D goldCoin;
	public Font alphaBetaFont;

	private GUIStyle hudStyle;

	void Start () 
	{
		healthAmount = 100;
		manaAmount = 100;

		hudStyle = new GUIStyle ();
		hudStyle.normal.textColor = Color.yellow;
		hudStyle.fontSize = 50;

	}

	void Update () 
	{
	
	}

	void OnGUI()
	{
		GUI.skin.font = alphaBetaFont;

		hudStyle.normal.textColor = Color.red;

		//Rendert Herz und den Wert der Lebensenergie des Spielers
	 	GUI.DrawTexture (new Rect (5, 5, 50, 50), heart);
		GUI.Label (new Rect (70, 5, 50, 50), healthAmount.ToString(), hudStyle);

		hudStyle.normal.textColor = Color.blue;

		//Rendert einen Manadrop und den Wert der Manaenergie des Spielers
		GUI.DrawTexture (new Rect (5, 60, 50, 50), manaDrop);
		GUI.Label (new Rect (70, 60, 50, 50), manaAmount.ToString(), hudStyle);

		hudStyle.normal.textColor = Color.yellow;

		//Rendert eine Goldmünze und den Wert der Anzahl der Münzen des Spielers
		GUI.DrawTexture (new Rect (5, 115, 50, 50), goldCoin);
		GUI.Label (new Rect (70, 115, 50, 50), goldAmount.ToString(), hudStyle);
	}

	//Holt den jetzigen Wert der Lebensenergie über welche der Spieler verfügt
	public void getHealthAmount(int amount)
	{
		healthAmount = amount;
	}
	
	//Holt den jetzigen Wert des Manas über welches der Spieler verfügt
	public void getManaAmount(int amount)
	{
		manaAmount = amount;
	}
	
	//Holt den jetzigen Wert des Goldes über welches der Spieler verfügt
	public void getGoldAmount(int amount)
	{
		goldAmount = amount;
	}
}

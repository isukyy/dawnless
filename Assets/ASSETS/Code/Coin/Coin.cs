﻿/*
 * Diese Klasse ist für das Item "Coin" zuständig 
 * Sie erfasst ein Kollision und führt SendMessage aus um dem Spieler Gold hinzuzufügen
 * 
 *(c) 2014-2016 by Alexander Chabowski
 */


using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour 
{
	public int amount;
	public AudioClip coinClip;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "SpriteManager" || collider.gameObject.tag == "Player" )
		{
			AudioSource.PlayClipAtPoint(coinClip, transform.position);
			GameObject.FindGameObjectWithTag ("SpriteManager").SendMessage ("addGold", amount);
			Destroy (gameObject);
		}
	}

}

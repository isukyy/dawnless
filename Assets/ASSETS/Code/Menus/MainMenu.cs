﻿/*
 * Diese Klasse ist für das Darstellen und die Bedienung des Hauptmenüs zuständig
 * 
 *(c) 2014-2016 by Alexander Chabowski
 */


using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour 
{
	/*
	 *0 = Singleplayer
	 *1 = Multiplayer
	 *2 = Options
	 *3 = Credits
	 *4 = End
	 */
	public GameObject[] menuBtns;
	public GameObject arrow;

	//Bestimmt welche Position der arrow haben wird
	private int arrowIndex;

	//Array welcher alle Positionen des Arrows in sich speichert
	public float[] arrowPosition;

	//Prüft ob Achse genutzt wird
	private bool axisIsInUse;

	//Gibt an welches Menü gerendert wird und welcher Input jeweils dafür abgefragt wird
	private int menuState;

	void Awake()
	{
		arrowIndex  = 0;
		menuState   = 0;
		axisIsInUse = false;
	}


	void Update () 
	{
		CheckInput();
		CheckArrowIndex();

	}

	private void CheckInput()
	{
		
		if(Input.GetKeyDown(KeyCode.W) || Input.GetAxis("Vertical") >= 0.2f && !axisIsInUse)
		{
			--arrowIndex;
			axisIsInUse = true;
		}
		
		if( Input.GetAxis("Vertical") == 0)
		{
			axisIsInUse = false;
		} 
		
		
		if(Input.GetKeyDown(KeyCode.S) || Input.GetAxis("Vertical") <= -0.2f && !axisIsInUse)
		{
			++arrowIndex;
			axisIsInUse = true;
		}

		if(arrowIndex == 0 && Input.GetKey(KeyCode.JoystickButton0) ||
		   arrowIndex == 0 && Input.GetKey(KeyCode.Space))
		{
			SceneManager.LoadScene("LoadSandbox");
		}

		
		if(arrowIndex == 2 && Input.GetKey(KeyCode.JoystickButton0)||
		   arrowIndex == 2 && Input.GetKey(KeyCode.Space))
		{
			//TODO: Optionen laden
		}

		if(arrowIndex == 4  && Input.GetKey(KeyCode.JoystickButton0)||
		   arrowIndex == 4 && Input.GetKey(KeyCode.Space))
		{
			Application.Quit();
		}
	}

	private void CheckArrowIndex()
	{
		if(menuState == 0)
		{
			if(arrowIndex == 0)
			{
				arrow.transform.position = new Vector3(arrow.transform.position.x,
				                                       arrowPosition[0],
				                                       arrow.transform.position.z);
				
				menuBtns[0].transform.GetComponent<Renderer>().material.color = new Color32(19,0,255,255);
				
			}
			else{menuBtns[0].transform.GetComponent<Renderer>().material.color = Color.white;}
			
			if(arrowIndex == 1)
			{
				arrow.transform.position = new Vector3(arrow.transform.position.x,
				                                       arrowPosition[1],
				                                       arrow.transform.position.z);
				
				menuBtns[1].transform.GetComponent<Renderer>().material.color = new Color32(19,0,255,255);
			}
			else{menuBtns[1].transform.GetComponent<Renderer>().material.color = Color.white;}
			
			
			if(arrowIndex == 2)
			{
				arrow.transform.position = new Vector3(arrow.transform.position.x,
				                                       arrowPosition[2],
				                                       arrow.transform.position.z);
				menuBtns[2].transform.GetComponent<Renderer>().material.color = new Color32(19,0,255,255);
			}
			else{menuBtns[2].transform.GetComponent<Renderer>().material.color = Color.white;}
			
			
			if(arrowIndex == 3)
			{
				arrow.transform.position = new Vector3(arrow.transform.position.x,
				                                       arrowPosition[3],
				                                       arrow.transform.position.z);
				menuBtns[3].transform.GetComponent<Renderer>().material.color = new Color32(19,0,255,255);
			}
			else{menuBtns[3].transform.GetComponent<Renderer>().material.color = Color.white;}
			
			
			if(arrowIndex == 4)
			{
				arrow.transform.position = new Vector3(arrow.transform.position.x,
				                                       arrowPosition[4],
				                                       arrow.transform.position.z);
				menuBtns[4].transform.GetComponent<Renderer>().material.color = new Color32(19,0,255,255);
			}
			else{menuBtns[4].transform.GetComponent<Renderer>().material.color = Color.white;}

			
			if(arrowIndex < 0)
			{
				arrowIndex = 4;
			}
			
			if(arrowIndex > 4)
			{
				arrowIndex = 0;
			}
		}
	}
}

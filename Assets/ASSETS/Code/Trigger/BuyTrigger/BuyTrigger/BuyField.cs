﻿/*
 * Diese Klasse erfasst ob sich der Spieler im jeweiligen Feld befindet 
 * um Lebensenergie zu kaufen insofern genug Gold vorhanden ist
 * 
 *(c) 2014-2016 by Alexander Chabowski
 */

using UnityEngine;
using System.Collections;

public class BuyField : MonoBehaviour
{
    //Der Preis des jeweiligen Feldes
	public int price;

    //Gibt an wie viel Lebensernergie mit dem jeweiligen Feld erworben werden kann
	public int heahlth;

    //Aktueller Goldwert des Spielers
	private int currentGold;

    //Wurde das Feld getriggert?
	private bool isEntered = false;

    //Der Collider des GameObject
    private Collider2D itemCollider;



    void Update()
	{
		//TODO: XBOX-Controller Taster zum kaufen hinzufügen
		if ( isEntered && itemCollider.CompareTag("SpriteManager") && Input.GetKey(KeyCode.Space)  && currentGold >= price
		    || Input.GetKey(KeyCode.JoystickButton0) && isEntered && itemCollider.CompareTag("SpriteManager") && currentGold >= price)
		{
			
			GameObject.FindGameObjectWithTag("SpriteManager").SendMessage("addHealth",heahlth, SendMessageOptions.DontRequireReceiver);
			GameObject.FindGameObjectWithTag("SpriteManager").SendMessage("decreaseGold",price, SendMessageOptions.DontRequireReceiver);

			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D _collider)
	{
        itemCollider = _collider;
		isEntered = true;
	}

	void OnTriggerExit2D()
	{
		isEntered = false;
	}


	public void getGoldAmount(int amount)
	{
		currentGold = amount;
	}
}

﻿/*
 * Läd bei Start ein Level Asynchron
 * 
 *(c) 2014-2016 by Alexander Chabowski
 */


using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevelAsync : MonoBehaviour
{
	public string levelname;

	void Start () 
	{
        SceneManager.LoadSceneAsync(levelname);
    }
}
